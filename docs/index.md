## FabZero Experiments



###  Qui suis-je ?

Je m'appelle Stéphanie, j'ai 36 ans. J'ai toujours rêvé de devenir institutrice et de travailler avec des enfants.

J'ai suivi des études de Physique à l'Université de Liège où j'ai réalisé un doctorat en Physique atomique. En 2011, après la soutenance de ma thèse, je suis partie un peu plus de deux ans à Boston (USA). Passionnée par le partage, le contact avec les jeunes et l'éducation, j'ai travaillé dans diverses organisations de diffusion des sciences telles que l'[_Edgerton Center_](https://edgerton.mit.edu/k-12) au MIT et l'association [_Science from Scientists_](https://www.sciencefromscientists.org/) basée à Boston. J'ai également enseigné dans une école d'été, la [Woman Technology Program](http://web.mit.edu/wtp-me/), qui acceuille des jeunes filles _highschoolers_  hyper motivées et passionnées par les sciences pour un programme intensif de 4 semaines à la découverte de l'ingénieurie mécanique, la _ME_, au MIT.

A mon retour en Belgique, j'ai travaillé pour l'association [Science et Culture](http://www.sci-cult.ulg.ac.be/), basée à l'Université de Liège, pendant 6 ans. En équipe, nous y mettions sur pied et presentions des spectacles scientifiques interactifs pour les élèves du primaire et du secondaire.

Aujourd'hui, je fais partie de l'équipe du [Fablab ULB](http://fablab-ulb.be/) où je suis en charge du projet **STEAM Lab**. Avec ma complice Floflo, nous développons ce projet de (futur) **laboratoire dynamique, ludique et bienvieillant** qui acceuillera des groupes de jeunes, enfants et adolescents, pour la réalisation de **projets STEAM (Science, Technology, Engeneering, Arts, Mathematics)** via des activités **d'enseignement par la recherche et par projets** au sein de la communauté du Fablab ULB.   

Mariée depuis 2010 à un aventurier, je suis aujourd'hui maman de trois enfants. J'ai toujours été passionnée par le théâtre et les arts de la parole. Durant ma jeunesse, j'ai suivi des cours passionants de déclamation, d'art dramatique et de _son, corps et voix_. Depuis maintenant 4 ans, je fais partie d'une chorale engagée appelée Charivari. Un peu avant la naissance de ma dernière fille, je me suis découverte une passion pour la couture et la confection de vêtements simples et confortables. J'aime recycler des vieux vêtements pour en créer de nouveau et ainsi créer des objets dans une démarche durable.  



![Stef_photo](images/stef.jpeg)
