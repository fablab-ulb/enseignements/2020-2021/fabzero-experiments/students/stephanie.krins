# 6. Electronique 2 - Construction d'une carte maison

Cette semaine, j'ai appris à fabriquer une carte Arduino maison.

Celle-ci contient un microprocesseur Atmel SAMD11C (32 bits) qui est directement capable de gérer l'USB. Il lui reste donc peu de mémoire, 16 kB seulement, pour d'autres programmes.

Pour la réaliser, j'ai suivi le "pas à pas" proposé par Nicolas De Coster [ici](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/electronics/-/blob/master/Make-Your-Own-Arduino.md).

##Etapes de montage

Ci-dessous, vous retrouverez les différentes étapes ainsi que les photos de ma propre carte :

 - Retrait du cuivre devant les connections USB :
 ![](../images/carte1.jpeg)

 - Placement et soudure de la puce Atmel SAMD11C. !!! Attention !!! il faut la placer dans le bon sens ! Quand le connecteur USB pointe vers la droite (comme sur l'image), l'écriture sur la puce doit être dans le bon sens (i.e. lisible de gauche à droite et le point repère sur la puce est alors en bas à gauche). Vérifiez plutôt deux fois qu'une...
 ![](../images/carte2.jpeg)

 - Placement et soudure du régulateur de tension et de la capacité qui    l'accompagne. Ils servent à avoir une tension de 3.3 V (l'USB donnant une tension de 5V).
 On met aussi de la soudure sur les connections USB et un adaptateur en plastique, collé au double face, à l'arrière de la connection USB.
  ![](../images/carte4.jpeg)

 - Vérification que tout se passe bien et que notre ordinateur reconnaît la carte quand on la connecte à un de ses ports USB.
 Dans mon cas sous Linux, je tape dans le terminal ```dmesg -w```. En effet, l'ordinateur a repéré le microcontrôleur dans le lecteur USB.
  ![](../images/carte_ordi.jpeg)

 - Soudure de la LED verte et de la résistance qui l'accompagne (cette dernière est manquante sur la photo ci-dessous mais elle a été placée juste à droite de la LED, son côté droit étant connecté à la PIN n°5). Pour vérifier le sens de la LED, j'ai vérifié avec un multimètre en mode "diode".
 ![](../images/carte5.jpeg)

## Test de la carte avec le programme blink
Le test consiste à faire clignoter la LED verte montée sur la carte.

Grâce à [SAMD CORE for arduino](https://github.com/mattairtech/ArduinoCore-samd), développé par MattairTech, on peut programmer notre carte en utilisant le Arduino IDE. La configuration de l'Arduino IDE pour pouvoir utiliser notre carte est détaillée en vidéos [ici](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/electronics/-/blob/master/Make-Your-Own-Arduino.md).

Une fois cette configuration en ordre, on utilise le programme Blink, permettant de faire clignoter une LED.

!!!Attention!!! Il faut cependant trouver la correspondance des pins de notre carte par rapport à ceux d'une carte Arduino. Ce tableau est repris ici :
![](../images/pinout.png)


La pin 5 sur notre carte SAMD11C correspond à la pin 15 pour un Arduino.
Le programme Blink que j'ai utilisé est donc :

```
void setup() {
  pinMode(15, OUTPUT);
}

void loop() {
  digitalWrite(15, HIGH);  
  delay(1000);                      
  digitalWrite(15, LOW);   
  delay(1000);                     
}
```

Voici le résultat en vidéo :

<video controls muted>
<source src="../../images/LED_carte.mp4" type="video/mp4">
Cette vidéo ne semble pas fonctionner dans votre browser.
</video>


## Soudure des autres éléments de la carte
J'ai ensuite soudé les derniers éléments de la carte.
![](../images/carte_finie.jpeg)

La carte étant, après cette opération, en court circuit, j'ai dû effectuer une recherche d'erreur(s).
J'ai nettoyé les pistes, repéré les bavures qui débordaient sur les pistes adjacentes. Avec un multimètre (en mode biiiiip), je suis partie à la recherche des courts circuits. J'ai vérifié les pistes qui devaient être connectées et celles qui ne devaient pas l'être en me référençant au plan ci-dessous :

![](../images/plan_carte.png)

J'ai fini par trouver que j'avais mal placé la résistance à côté de la LED rouge. Au lieu d'être à cheval entre la capacité et la LED rouge, elle était reliée à la masse. Ce problème a été corrigé.

La carte a cependant du mal à être repérée par mon ordinateur... Affaire à suivre... 
