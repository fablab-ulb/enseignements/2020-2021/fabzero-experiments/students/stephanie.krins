# 4. Découpe assistée par ordinateur

Cette semaine j'ai appris à utiliser un laser cutter et une découpeuse vinyle.


## 4.1 Laser cutter BEAMO de chez FLUX

Le laser cutter que j'ai appris à utiliser est le BEAMO de chez FLUX. Cette découpeuse laser présente l'avantage d'être transportable. Elle possède un système de refroisissement et de ventilation intégré. On peut donc l'utiliser tel quelle à condition de placer le tuyau d'évacuation d'air à l'extérieur (ou bien dans un système de filtration).

Pour en connaître plus sur le produit, voir [ici](https://www.fluxlasers.com/beamo.html).

### Spécificités
 - Surface de découpe : 30 x 21 cm
 - Hauteur maximum : 4.5 cm
 - Puissance du LASER : 30 W
 - Type de LASER : Tube CO2 (infrarouge)

 Ce tableau résume les spécificités des différentes découpeuses laser FLUX, en bleu, la BEAMO :
 ![BEAMO](../images/beamo.jpg)

Le manuel d'utilisation est téléchargeable [ici](https://cdn.webshopapp.com/shops/290919/files/316242658/beamo-user-manual-20191203.pdf).

### Le logiciel Beam Studio
Pour faire fonctionner la découpeuse laser à partir de son ordinateur, on doit installer le logiciel Beam Studio. Il est disponible pour Windows, Linux et Mac. Une version pour Smartphone est également disponible. Elles sont téléchargeables [ici](https://www.fluxlasers.com/downloads/).

### Mise en route  et calibration
Pour mettre en route la découpeuse laser, nous avons suivi la procédure proposée par le fournisseur qui est détaillée [ici](https://support.flux3dp.com/hc/en-us/articles/360000948955-II-Unboxing-beamo).

Une fois l'ordinateur et le laser cutter connectés entre eux, nous avons choisi de réaliser une grille de calibration pour savoir quels paramètres de puissance et quelle vitesse utiliser pour découper et pour engraver.

Nous avons réalisé des matrices de calibration de ce type :

![calib](../images/calib.jpg)

Les matrices de 5x5 (25 carrés de couleurs différentes) sont réalisées dans Inkscape et sauvées en format vectoriel *Plain SVG* lisible par BEAMO Studio. Le canevas est disponible [ici](../files/LaserCutter/Calibration.svg).

Dans le logiciel BEAMO Studio, on attribue ensuite à chaque couleur une valeur de puissance ou _Power_ et de vitesse ou _Speed_.
La puissance du laser s'exprime en % de la puissance totale.
La vitesse du laser est exprimée en mm/s, la vitesse maximale étant de 300 mm/s.


NB : Attention, une fois les matrices importées dans Beam Studio, pour voir les différentes couleurs, il faut se placer en mode "couleurs". Dans l'onglet View, cliquez sur "Use Layer Color" comme ci-dessous :

![couleurs](../images/couleurs.jpg)


Nos grilles de calibration pour le logiciel BEAM Studio sont disponibles ci-dessous (sur ces grilles la vitesse S et la puissance P sont exprimées en pourcents) :

 - [Calibration P(20-100) S(20-100)](../files/Lasercutter/Calibration0-100.beam)  
 - [Calibration P(10-50) S(10-50)](../files/Lasercutter/Calibration0-50.beam)  
 - [Calibration P(60-100) S(60-100)](../files/Lasercutter/Calibration50-100.beam)
 - [Calibration P(5-25) S(2-10)](../files/Lasercutter/Calibration5-25.beam)  
 - [Calibration P(10-30) S(30-50) ](../files/Lasercutter/Calibration10-30.beam)  



### Faire le focus du laser sur la surface

Avant d'effectuer une tâche avec la découpeuse laser, il faut absolument régler le focus du laser.
Pour se faire il faut descendre le morceau de plexi, dévisser la tête dorée et redscendre la tête jusqu'à ce que le plastique touche la surface de travail. Cette étape est à refaire avant toute découpe sur un nouveau support.


![focus](../images/strip1.png)

### Résultats obtenus

Nous avons constaté que pour le laser BEAMO utilisé avec une feuille de papier cartonnée d'épaisseur, nous devons utiliser les paramètres suivants :

 - pour engraver P = 15 %, S = 8 % = 24 mm/s ;
 - pour couper P =   20 %, S = 8 % = 24 mm/s.

![results](../images/resultatsLC.jpg)

## 4.2 Découpeuse vinyle Silhouette Cameo 4

J'ai choisi de découper avec la Silhouette Cameo 4 un sticker pour décorer la boîte à tartines de mon fils.

Le logiciel à utiliser pour interagir avec les machines de chez Silhouette est le _Silhouette Studio_. Une version gratuite, pour Windows ou Mac, est disponible sur le site Silhouette America, [ici](https://www.silhouetteamerica.com/software).

### Design du sticker

Le sticker que j'ai choisi est composé d'une image vectoriel de fusée (image trouvée sur le Silhouette Design Store) et du prénom de mon fils. Je souhaite avoir le hublot de la fusée de la même couleur que le prénom. Je choisis de découper la fusée dans un vynile à paillettes gris et le hublot et le prénom dans un vynile brillant jaune fluo.


![fusee](../images/fusee.png)

La première étape consiste à choisir les bonnes dimensions du sticker. Cela se fait dans l'onglet DESIGN du programme.

Une fois cela fait, je prépare le fichier pour la découpe. Je sais que je vais devoir couper le hublot et le prénom séparément de la fusée (puisque je souhaite un sticker à deux couleurs).

J'ajoute autour des différents objets à découper des rectangles. Ils me permettront de faire un travail plus propre. Je découperai ces rectangles au travers du papier de transfert du vinyle.

Je choisis des couleurs de contours différentes pour les contours des rectangles, de la fusée et du hublot et du prénom (vous comprendrez plus loin pourquoi, cela va me faciliter les choses quand on passera à la découpe)


![fusee](../images/fusee2.png)

### Découpe du sticker

#### Découpe de la fusée

Je choisis de découper d'abord la fusée.

Dans l'onglet ENVOYER du programme, je clique sur l'onglet Ligne qui me permet de séparer les différents contours de couleur du dessin.
Pour chaque couleur, je définis l'action à réaliser et le matériau à travers lequel la lame devra couper.

![fusee](../images/fusee3.png)

On a :

 - En noir :  matériau = Vinyle à paillets, outil = la lame automatique, action = couper
 - En mauve, matériau = papier de transfert (la couche en papier sous le vinyle), outil = lame automatique, action = couper.

 J'avais déjà découpé ce vinyle à paillettes précédemment, je sais donc que les valeurs de _Profondeur de lame_, _Force_ et _Vitesse_ proposées par défaut par le programme fonctionnent.

 Je lance  la découpe en cliquant sur ENVOYER.

![fusee](../images/vinyle_cut.jpeg)

La découpe s'est bien passée. La lame a bien découpé le vinyle uniquement autour de la fusée et est passée à travers le papier de transfert sur le rectangle. Il reste  à écheniller avec un crochet adéquat.

![fusee](../images/strip2.jpeg)

#### Découpe du hublot et du prénom

Je m'attaque ensuite au hublot et au prénom. Ils seront découpés sur du _vinyle brillant_ (option préencodée). Pour les cadres verts, je choisis le matériau _papier transfert d'image_.

![prenom](../images/prenom.png)


 N'ayant jamais fait de découpe avec ce vinyle fluo, je fais un test des paramètres préenregistrés pour la découpe du vinyle brillant.

 Le premier test avec les valeurs préenregistrées a échoué !

 ![testvinyle](../images/strip3.jpeg)

J'ai  augmenté la profondeur de lame (passage de 1 à 2) et la force (passage de 10 à 13).
Cette fois le test à réussi.
C'est avec ces dernières valeurs de que vais découper le prénom et le hublot.

 ![testinyle2](../images/strip5.jpeg)

#### Superposition de la fusée et du hublot

Le sticker fusée que j'ai choisi est en deux couleurs.
La fusée a été découpée dans un vinyle à paillettes gris, le hublot dans un vinyle jaune fluo.
Le hublot doit venir se placer au dessus de la fusée pour former le sticker final bicolore.

#####Comment procéder ?

Pour la superposition des deux vinyles, on va avoir besoin d'un morceau de "papier collant de transfert".
_Ce papier collant est, comme son nom l'indique, utilisé pour transférer le sticker depuis le papier blanc sur lequel il est fourni vers la surface à laquelle il est destiné_.

 1. Une fois le hublot découpé et échenillé, je le place au milieu du "papier  de transfert".
 _NB : Pour transférer le vinyle (le hublot ici) sur le collant de transfert, on frotte avec un scraper ou une carte (genre carte de banque) puis on retire délicatement le papier de support blanc du vinyle._

 2.  Une fois le hublot seul sur son papier de transfert, on le positionne AU BON ENDROIT sur le sticker fusée.

 3.  On frotte de nouveau avec la carte pour assembler le tout.
 Il reste à retirer délicatement la papier de support blanc du vinyle fusée.
 Pour réussir facilement cette opération, un bon truc est de plier ce papier blanc à 180 ° puis de le retirer délicatement comme montré sur la photo ci-dessous :
  ![truc](../images/truc_transfert.jpeg)

4.  Le résultat obtenu est un sticker bicolore (sur un papier de transfert) près à être transféré sur un support.

#### Transfert du sticker sur la boîte à tartines

Je positionne le sticker à l'endroit désiré sur la boîte à tartines de Robinson :


![transfert boite](../images/transfert_boite_1.jpeg)

De nouveau, avec une carte, j'applique le sticker sur la boîte.
Délicatement, je retire le papier collant de transfert.
Le sticker doit coller sur la boîte et plus sur le papier transfert.

![transfert boite](../images/transfert_boite.jpeg)

 Je fais la même opération avec le prénom fluo...
 et...
 TADAAAAM !!!
La boîte est décorée, ça lui fait une nouvelle jeunesse !
 Merci qui ??? ^^

 ![transfert boite](../images/sticker_fini.jpeg)
