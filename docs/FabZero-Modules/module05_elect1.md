# 5. Electronique 1 - Arduino

Cette semaine, Floriane et moi avons découvert l'Arduino. Nous avons réalisé un circuit électronique pour faire clignoter une LED et un autre pour simuler des feux de signalisation. Enfin, nous avons effectué une mesure de température avec un capteur ad hoc.

##L'arduino Uno

L'Arduino UNO est un microcontrôleur programmable. Il peut être programmé via la logiciel IDE d'Arduino (téléchargeable [ici](https://www.arduino.cc/en/software/)) et donc être contrôlé par un ordinateur. Le langage de programmation utilisé est le C. Il est relié à l’ordinateur grâce à un câble USB. Pour en savoir plus, vous pouvez lire [ici](https://www.arduino-france.com/review/arduino-uno/).

L'un des avantages de ce microcontrôleur est sa facilité d'utilisation qui le rend accessible à tous.
Il existe une multitude de programmes préenregistrés qui nous permettent de nous faire la main en tant que débutant.
Il existe également de nombreux tutoriels sur internet. J'ai découvert [celui-ci](https://www.arduino-france.com/tutoriels/comment-debuter-arduino/) qui explique la base à connaître pour comprendre ce que l'on fait.

Floriane et moi même nous sommes faites la main sur le _Beginner Kit_. Nous avons construit les trois premiers projets :
 - Faire clignoter une LED ;
 - SOS avec une LED; et
 - Les feu de signalisation.

## A blinking LED


 Le programme "Blink" est un classique pour se faire la main sur un circuit électronique. Il permet de faire clignoter une LED.

 Pour encore plus de facilité, on peut utiliser la LED déjà placée sur l'Arduino. On voit sur le "pin mapping" que la LED est accessible via la PIN DIGITALE 13, à droite sur le plan ci-dessous.

 ![pin](../images/arduino_pin.jpeg)

 Dans le Arduino IDE, on écrit notre programme.

> Un programme est constitué de deux parties :
> - la fonction _void setup()_  qui sert d'initialisation de la carte, elle est appliquée une fois ;
> - la fonction _void loop()_ qui est la partie principale du programme, elle est lue en boucle.
>
>Les programmes se présentent ainsi :
>```
>void setup() {
>  // put your setup code here, to run once:
>
>}
>
>void loop() {
>  // put your main code here, to run repeatedly:
>
>}
>```

Pour la LED clignotante, dans la partie void setup(), on indique qu'on va travailler avec la LED de la carte, la pin digitale 13 qui sera considérée comme une sortie.

Dans la partie void loop(), on doit alimenter puis éteindre la LED autrement dit mettre la sortie digitale 13 haute et puis basse. Pour qu'on puisse voir le passage entre ON et OFF, on ajoute un délai d'une seconde (1000 ms) entre les positions hautes et basses.

```
void setup() {
pinMode(13, OUTPUT);
}
void loop() {
digitalWrite(13, HIGH);
delay(1000);
digitalWrite(13, LOW);
delay(1000);
}
```
## A traffic light

Le second projet que nous avons réalisé est le feu de signalisation interactif.
Il est expliqué en détails sur ce [blog](https://www.dfrobot.com/blog-595.html).
Nous avons effectué le montage suivant :

![traffic](../../images/traffic_light.png)

Nous avons ensuite lu et compris le programme proposé sur le blog plus haut.
Voici le résultat obtenu :

<video controls muted>
<source src="../../images/traffic_light.mp4" type="video/mp4">
Cette vidéo ne semble pas fonctionner dans votre browser.
</video> 

## Utilisation d'un nouveau capteur

Lorsqu'on utilise un nouveau capteur pour notre Arduino, il est important d'aller consulter la documentation de ce dernier.
Les capteurs à notre disposition ont été répertoriés [ici](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/electronics/-/blob/master/IO-Modules.md).

Dans notre cas, nous avons utilisé ce [capteur de température](https://www.velleman.eu/products/view/?id=439184).

Dans les informations disponibles en ligne, on trouve dans les "téléchargements"  la notice d'emploi, la fiche d'information et aussi les _libraries_ ou _bibliothèques logiciel_.

![](../images/capteur.jpeg)

Dans les bibliothèques, on trouve une collection de routines, des séquences d'instruction déjà écrites, prêtes à être utilisées dans un programme. Ces codes préécrits permettent de faire fonctionner le capteur.

En pratique, on télécharge les fichiers .ZIP de la bibliothèque logiciel.
 Ensuite on peut directement aller les chercher dans le programme IDE Arduino :  onglet Croquis / Inclure une bibliothèque / Ajouter la bibliothèque .ZIP / (on va chercher le .ZIP téléchargé)

![](../images/biblio.jpeg)

Ensuite les routines se retrouvent dans les exemples : onglet Fichier/ Exemples / (le capteur qu'on veut) / (la routine qu'on veut)

![](../images/exemple.jpeg)
