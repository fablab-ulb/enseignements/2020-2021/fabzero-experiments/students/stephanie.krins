# 1. Documentation

Cette semaine j'ai travaillé sur mon "idea board" pour le projet final et j'ai tenté de me familiariser avec le processus de documentation.

### Installation des programmes pour le cours FabZero
 Comme je commence de zéro l'apprentissage des outils de ce cours, j'ai choisi de travailler sous **linux** (que je ne connais pas du tout). On m'a plusieurs fois parlé de la puissance et de la rapidité de cet outil donc je me suis dite que c'était l'outil à apprendre à utiliser.

#### Tout en une fois ! ou THE script .sh de Nico
Dans un premier temps, j'ai installé tous les programmes dont on aura besoin pour ce cours FabZero. L'installation de tous ces programmes se réalise en une seule fois ! J'étais déjà impressionnée et curieuse de voir comment cela allait être possible...

L'installation se fait en utilisant le script _x_ubuntu_PostInstall.sh_ crée par Nico  et trouvé sur le FabCloud [ici](https://github.com/fabfoundation/linux/blob/master/x_ubuntu_PostInstall.sh)

![progNico](../images/progNico.jpg)

Pour pouvoir faire tourner ce script dans le terminal de mon ordi, il a fallu :
  - copier le contenu du fichier _.sh_ dans un fichier texte (pour ça il suffit de visualiser le fichier en _raw_ (voir image plus haut) et de copier/coller le texte dans un fichier .txt),
  - enregister ce fichier texte avec une extension _.sh_ dans le dossier FabZero qui se trouve sur mon ordi (je l'ai appelé *_programmes.sh_*)
  - le rendre _exécutable_. Pour ça, j'ai dû changer les permissions du fichier : click droit, _propriétés_, onglet _permissions_, cocher la case "Autoriser l'exécution du fichier comme un programme" (comme ici en bas).

![permission](../images/permission.jpg)

Ensuite il faut _"démarrer le terminal à la racine du script"_  (c'est ce que j'ai lu dans les commentaires du programme de Nico, regardez dans l'encadré bleu deux images plus haut)
...
ok ok... Cela signifie que je dois lancer le terminal depuis l'endroit où est mon fichier _programmes.sh_ sur mon ordi. Check !

Je note dans le terminal la commande pour lancer le script. Check !
```
sudo ./programmes.sh --all
```
Il me demande mon mot de passe, je le donne...

![programme_lancement](../images/programmessh.jpg)

Ca travaille ! ... C'est fait !!!

Tout n'a pas fonctionné du premier coup...

![progFix](../images/programFix.jpg)
Pas grâve, je tape (comme suggéré dans le terminal)
```
sudo apt --fix-broken install
```
Ca fonctionne ! Youhouhou !!!

Pour essayer j'ouvre _atom_ - ok ca marche - youhou

J'ouvre _openscad_ - ok ça marche -reyouhou

J'ouvre _gimp_ - KO - message d'erreur - il n'a pas trouvé _gimp_

![atom](../images/atom_gimp.jpg)

 Pour résoudre le probléme, il propose une solution, je tape
 ```
 sudo apt install gimp
 ```
Ok c'est bon ! Youhouhou !

Allez, on continue, j'ouvre _inkscape_ - KO - pour résoudre le probléme je tape
```
sudo apt install inkscape
```
Je retente d'ouvrir - Message d'erreur

![erreurInkscape](../images/erreurInkscape.jpg)

 Je résouds ce problème en cherchant sur google. En tapant
 ```
 sudo apt install libcanberra-gtk0 libcanberra-gtk-module
 ```
 ça fonctionne, youhouhouhou !


Allez, pour finir, pour pouvoir documenter tout ce que je viens de faire, j'essaie d'ouvrir _simplenote_ - KO - il n'est pas dans le liste de programmes de base. OK, c'est pas grave, pour l'installer je tape, comme suggéré

```
sudo snap install simplenote
```
![simplenote](../images/simplenote.jpg)

Ca fonctionne mais il me reste une erreur irrésolue...

![simplenote2](../images/simplenote2.jpg)

On verra ça plus tard...

**Danse de la joie car les programmes sont installés !!!**

### Activation notifications sur GITLAB
Je suis les recommandations trouvées dans le cours [ici](https://docs.gitlab.com/ee/user/profile/notifications.html#editing-notification-settings). Je choisis d'activer les notifications pour le cours FabZeroExperiments. (capture d'écran)

### Installation de GIT sur mon ordi
_NB : (à ne pas lire si vous êtes intéressé uniquement par le côté technique de ce site, en même temps, si tel est le cas, je vous conseille de consulter le  [site](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/clementine.benyakhou/modules/module01/) réalisé par une autre étudiante de FabZero Design, Clémentine, qui a super bien référencé ce que je vais essayer de décrire plus bas ) Bon je dois bien dire que jusque là, ça avait encore été. Ce que j'avais entrepris avait fonctionné. J'avais trouvé qu'en effet Linux avait cette puissance d'installer tous les programmes en une seule fois, c'était beau, c'était grand, c'était fort. Je m'en allais donc confiante dans la suite de l'exploration du grand labyrinthe qu'est le site du cours FabZero et les tutos du Gitlab...  un endroit dans dans lequel on pourrait, si on n'y prend pas garde, se perdre sans ne plus **JAMAIS** pouvoir retrouver notre chemin !_

Viens alors le moment d'installer GIT sur mon ordinateur et puis de mettre en place le clone de mon "repo"...

_A partir de ce moment-là, l'aventure dans l'inconnu commence pour moi... Vais-je m'en sortir ? Vais-je pouvoir installer ce clone tant espéré... Le suspence est TOTAL..._

Pour installer le GIT sur mon appareil, je suis les recommandations trouvées [ici](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#install-git).

Je vérifie d'abord si j'ai une version GIT installée

 ![simplenote](../images/git_version.jpg)
Apparemment, j'en ai une ! La version 2.25.1 !  Youhou ! Sûrement grâce au programme de Nico ?!
Je configure le GITLAB comme renseigné dans la doc.

### The SSH key
Ensuite, je m'occupe de la _méthode d'authentification_. Si j'ai bien compris, pour pouvoir avoir accès à gitlab depuis mon ordi, je dois mettre en place une clé SSH. Une clé privée sur mon ordi, une clé publique sur gitlab. Entre ces deux clés, un chemin qui permettra de relier l'ordi et le gitlab...
Pour créer la clé SSH, je suis les recommandations trouvées [ici](https://docs.gitlab.com/ee/ssh/README.html#ed25519-ssh-keys).
Il faut d'abord _"go to the .ssh subdirectory"_. Pour trouver mon .ssh subdirectory, j'ai tapé dans le terminal
```
cd ~/.ssh
```
_cd_ pour _change directory_.

Une fois en place, je génère la clé SSH en tapant
```
ssh-keygen -t ed25519 -C "cle SSH"
```
En vrai, ça donne ça :

![ssh](../images/ssh.jpg)

La clé SSH a été crée sur mon ordi, dans le dossier _home_, sous dossier _Stef_, sous-dossier _ssh_. Je note la position pour pouvoir la retenir !

Une fois la clé SSH crée sur mon ordi, je dois la copier sur le gitlab account et je l'ai appelée du joli nom _SSH Key Home Workstation_. J'ai suivi les instructions trouvées [ici](https://docs.gitlab.com/ee/ssh/README.html#ed25519-ssh-keys).

### The Clonage du GIT via SSH sur mon ordi
Le mode d'emploi est par [ici](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-via-ssh).
Avant tout et pour ne pas faire de bêtises, il faut **ouvrir le dossier de l'endroit où vous voulez cloner votre espace gitlab sur votre ordinateur** ! (Dans mon cas ce sera dans un sous-dossier de ma Dropbox appelé _FabZero_).
Une fois dans ce dossier, ouvrez le terminal et tapez
```
git clone xxx
```
Le xxx sera remplacé par ce que vous aurez copié en cliquant sur le boutons _CLONE_ de votre espace sur le Gittlab, ici en vert.

![adresse](../images/clone2.jpg)

Pour le terminal, ça donne donc :

![clone](../images/clone.jpg)

Et TADAAAAAAM, un nouveau dossier est crée dans la DB au bon endroit avec les mêmes fichiers que sur le Gitlab ! C'est pas beau ça ???

Maintenant on peut bosser sur l'ordi et puis, quand on veut "on push sur le gitlab" !

### Pull and Push
Avant de commencer un travail dans notre repository cloné, par exemple l'écriture d'une documentation pour le cours, on "PULL" les derniers changements de notre repo du GIT (en ligne) sur notre copie locale dans notre ordi.


La commande à utiliser dans le terminal est :  
```
git pull
```
NB: le terminal est lancé depuis l'endroit où se trouve notre repository local sur notre ordi.
Dans mon cas, je lance le terminal dans ma _Dropbox_, dans mon ficher _ULB_Stef_, dans le fichier _FabZero_, dans le sous dossier _stephanie.krins_ (voir capture d'écran ci-dessous).

Quand on a fait ce qu'on voulait faire sur son ordi, on ADD, COMMIT et puis PUSH sur le GIT en ligne.
Les commandes à utiliser dans le terminal sont respectivement :

```
git add -A
```
```
git commit -m "avec une description de l'action"
```
```
git push
```

Comme vu ici : 
![push](../images/push.jpg)

Danse de la joie !!!!! Oyé Oyé Oyé !!!! Oh YEEEEAAAAHHHH !!!
