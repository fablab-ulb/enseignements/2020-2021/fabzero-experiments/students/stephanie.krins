# 3. Impression 3D



## Réalisation d'un kit de flexlinks
### Trois premiers Flexlinks
Les flexlinks que j'avais codé en Openscad la semaine dernière [ici](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/stephanie.krins/FabZero-Modules/module02/) ont été convertis en fichiers .stl pour pouvoir être imprimé en 3D.

Les fichiers .stl sont disponibles ici :
 - [fixed-fixed beam](../files/FLEXLINKS/fixed-fixedBeam_lego.stl)
 - [cantilever beam](../files/FLEXLINKS/Fixed_fixedBeam_HalfCircle_lego.stl)
 - [fixed-fixed beam Half Circle](../files/FLEXLINKS/Fixed_fixedBeam_HalfCircle_lego.stl)
 - [width_hole_test_bench](../files/FLEXLINKS/width_hole_test_bench.stl)

### Design d'un quatrième flexlink à partir du code de Floriane
J'ai complété mon kit de Flexlinks par un quatrième, le _Cross-Axis Flexural Pivot_  [à voir ici](https://www.compliantmechanisms.byu.edu/flexlinks).

Pour l'obtenir, je suis partie du code de la deuxième pièce de Floriane, la [_Cross-Axis Flexural Pivot_Floflo_](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/floriane.weyer/FabZero-Modules/module02/).

J'ai utilisé son code pour définir une pièce en "Z". J'ai ensuite construit une pièce en "X" en superposant deux pièces en "Z", la seconde étant tournée de 180° autour de l'axe x puis translatée en y au dessus de la première.

Voici le code, disponible [ici](../files/FLEXLINKS/Cross_Axis_Flexural_Pivot.scad).

```
// file name : Cross_Axis_Flexural_Pivot.scad

//  AUTHOR : Stephanie Krins (stefkrins@gmail.com)

//  DATE   : 2021-03-02

//  LICENSE : Creative Commons [CC BY-4.0](https://creativecommons.org/licenses/by/4.0/).

// Code Adapted from the "Cross-Axis Flexural Pivot" of Floriane Weyer
//(https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/floriane.weyer/FabZero-Modules/module02/)

// PRINCIPALES MODIFICATIONS :
// Je garde le code crée par Floriane pour définir une pièce en Z
//Je crée ensuite une pièce "en croix" en supperposant à une pièce en Z, une seconde pièce en Z, tournée de 180° selon l'axe x puis translatée en y.

// Paramètres
$fn=50;

// Hauteur de la pièce en Z
height = 5;

// Nombre de trous de la pièce en Z
n = 4;
// Rayon interne des trous
radius = 2.5;
// Distance centre à centre entre deux trous
distance = 8;
// Distance entre l'extérieur du trou et l'extérieur de la pièce
edge_thickness = 2;
//Longueur de la tête
head_length = (n-1)*distance + 2*edge_thickness + 2*radius;


//Distance selon y entre les deux têtes
head_distance = 40;

// Epaisseur de la tige
beam_thickness = 2;

// Calcul de la longueur de la tige et de l'angle
beam_length = sqrt(pow(head_distance,2)+pow((n-1)*distance,2))-(2*radius);
angle = atan(head_distance/((n-1)*distance));


//définition de la pièce en Z
module Z_flexlink(){
// Création de la première tête
head();
// Création de la deuxième tête
translate([0,head_distance,0]){
    head();
    }    
// Création de la première tige entre les deux têtes   
rotate([0,0,angle])   
translate([radius,-beam_thickness/2,0])
cube([beam_length,beam_thickness,height], center = True);}

//Création de la pièce en croix (superposition de deux pièces en Z en miroir)
// La pièce en Z n°1
Z_flexlink();
// La pièce en Z n°2
//Cette seconde pièce est tournée par rapport à la première de 180° selon l'axe x
//et translatée en y de la distance entre le deux têtes (pour être parfaitement superposée à la pièce en Z n°1)
rotate(a=180, v=[1,0,0])
translate([0,-head_distance,0])
Z_flexlink();


//Définition du module head
module head(){
    difference(){
        hull(){
            cylinder(h = height, r = radius+edge_thickness, center = True);
            translate([(n-1)*distance,0,0]){cylinder(h = height, r = radius+edge_thickness, center = True);}
            }
        for (i = [1:n]){
            translate([(i-1)*distance,0,0]){
                cylinder(h = height, r = radius, center = True);
                }
            }
        }
    }
```
![Cross_Axis_Flexural_Pivot](../images/Cross_Axis_Flexural_Pivot.jpg)

Le fichier .stl de cette pièce est [ici](../files/FLEXLINKS/Cross_Axis_Flexural_Pivot.stl).
## Impression 3D du kit de Flexlinks

Pour imprimer les pièces en 3D, nous travaillons avec le logiciel Slicer de Prusa. Pour l'installer sur linux, vous tapez dans le terminal :
```
sudo snap install prusa-slicer
```
Un fois dans le programme, vous _importez_ les fichiers .stl :
>Menu Fichier, Importer, Importer STL/OBJ/...



Voici mon kit à imprimer :

![prusa](../images/prusa.jpg)

Ensuite, il nous faut définir plusieurs paramètres avant de lancer une impression.

NB : Pour une impression au FabLab ULB, voici la [marche à suivre](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/class/-/blob/master/vade-mecum/3D_print.md).
### 1. Choisir l'imprimante et le filament
Les imprimantes disponibles au Fablab sont des Prusa I3MK3 ou I3MK3S avec une buse de 0.4 mm (une seule fonctionne avec une buse de 0.6 mm, c'est marqué sur l'imprimante).


A la maison, j'ai une imprimante PRUSA MINI avec une buse de 0.4 mm qui utilise du filament PETG. Pour ajouter cette imprimante dans le programme Slicer :

>Menu Imprimante, Ajouter/Supprimer des imprimantes

On arrive dans l'assistant de configuration des imprimantes et on va chercher l'imprimante qui nous convient, pour moi :

![prusamini](../images/prusamini.jpg)


Après avoir choisi le _type d'imprimante_ dans le slicer, on choisit les _règlages d'impression_ et le _type de filament_.

Ici l'imprimante choisie est une Prusa Mini.
Le règlage d'impression est toujours la moitié de la taille de la buse, soit ici 0.2 mm.
Le filament est ici du PETG.


![impression](../images/imprimante.jpg)
###  2. Règlages d'impression
#### Couches et périmètres
La hauteur de couche est choisie égale à la moitié de la taille de la buse, idem pour la hauteur de la première couche.

![impression2](../images/couches.jpg)

#### Remplissage
Généralement on utilise un remplissage de 10 ou 15 %, maximum 35 %.
Le modèle de remplissage est au choix selon ce qu'on souhaite obtenir. Ici, j'ai choisi du nid d'abeille.

![impression2](../images/remplissage.jpg)

#### Jupe et bordure
La jupe représente le périmètre d'impression de l'objet. Avant de commencer l'impression proprement dire, l'imprimante dessine un périmètre à l'intérieur duquel l'impression va avoir lieu.

Ici la jupe se fera en 1 passage à 2 mm de l'objet imprimé.

J'ai choisi de ne pas mettre de bordure autour des pièces (car les pièces sont de faibles hauteurs). Par contre, j'en enduit la plaque de l'imprimante de colle PRITT pour éviter un soulèvement des pièces pendant l'impression.

![impression3](../images/jupe.png)

### 3. Création du G-code
Une fois les paramètres réglés, on exporte le G-code qui est le langage de l'imprimante 3D et on lance l'impression.

![impression](../images/impression.jpg)

### Résultats de l'impression
Voici le kit imprimé :
![kit](../images/kit.jpeg)

## Mécanisme avec les Flexlinks
J'ai choisi de filmer un mécanisme très  simple.

Deux flexlinks rectilignes (en orange) sont placés perpendiculairement à deux pièces plates LEGO (en noir).
La présence des deux flexlinks parallèles permet une translation des deux pièces de légo l'une par rapport à l'autre.
Si on ne place qu'un seul flexlink, on obtient alors la rotation d'une pièce par rapport à l'autre.

![meca_flex](../images/kit.jpeg)

<video controls muted>
<source src="../../images/meca_flex.mp4" type="video/mp4">
Cette vidéo ne semble pas fonctionner dans votre browser.
</video> 

>Note : Compression de vidéos
>
>Pour compresser la vidéo ci-dessus, j'ai installé le programme _ffmpeg_ sur linux.
>
>Pour le faire, on tape dans le terminal :
>```
>sudo apt install ffmpeg
>```
>Ensuite pour compresser la vidéo, appelée ici _input_video.mp4_, on ouvre le terminal dans le dossier où se trouve la video et on tape la commande suivante :
>
>```
>ffmpeg -i input_video.mp4 -vcodec libx264 -crf 25 -preset medium -vf >scale=-1:720 -acodec libmp3lame -q:a 4 -ar 48000 -an output_video.mp4
>```
>La video sortante _output_video.mp4_ fait alors moins de 10 MB.
