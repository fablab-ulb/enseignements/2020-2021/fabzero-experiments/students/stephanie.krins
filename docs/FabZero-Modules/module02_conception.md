# 2. Conception Assistée par Ordinateur

Cette semaine j'ai appris à utiliser le logiciel de dessin 3D OpenScad pour réaliser un kit de [FlexLinks](https://www.compliantmechanisms.byu.edu/flexlinks).  

Je me suis posée la question de savoir à quoi pouvait bien servir les FlexLinks. J'ai trouvé cette vidéo de _Veritaserum_ qui est  très inspirante et qui a répondu à pas mal de mes questions.

<iframe width="560" height="315" src="https://www.youtube.com/embed/97t7Xj_iBv0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Il y a encore plein d'autres exemples [ici](https://www.compliantmechanisms.byu.edu/videos).

## Installation de OpenScad et Freecad

Grâce à l'[installation globale des programmes](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/stephanie.krins/FabZero-Modules/module01/) que j'ai réalisé la semaine dernière, le programme Openscad était déjà installé sur mon ordinateur. Si ce n'est pas le cas pour vous, pour l'installer sous Linux, on tape dans le terminal :
```
sudo apt-get install openscad
```

J'ai également installé le logiciel Freecad (qui n'était pas dans le kit de programmes de Nico). J'ai suivi les conseils donné durant le cours par Nico et j'ai installé la version [Freecad Daily](https://wiki.freecadweb.org/Download/cs#Ubuntu_Daily_PPA_Packages).

_NB : Plus de détails sur le site FabAcademy de Nicolas qui est une source hyper intéressante pour les utilisateurs de Linux, c'est par [ici](http://archive.fabacademy.org/2018/labs/fablabulb/students/nicolas-decoster/?page=assignment&assignment=02)._

## Réalisation des premiers FlexLinks avec Openscad
###Fixed-fixed beam
Le premier FlexLink que j'ai choisi de réaliser est le ["Fixed-Fixed Beam"](https://www.thingiverse.com/thing:3020736).

Pour le réaliser dans OpenScad, voici le code que j'ai utilisé. Il est disponible [ici](../files/FLEXLINKS/fixed-fixedBeam_lego.scad).
```
//  FILE   : fixed-fixedBeam_lego.scad

//  AUTHOR : Stephanie Krins (stefkrins@gmail.com)

//  DATE   : 2021-02-23

//  LICENSE : Creative Commons [CC BY-4.0](https://creativecommons.org/licenses/by/4.0/).


//flexlink "fixed-fixed beam" from the BYU website
//(https://www.compliantmechanisms.byu.edu/flexlinks)
//2 connectors (with 2 holes) connected with 1 beam

//////////////
//DEFINITIONS
/////////////
thickness_connector = 6.4; //in z direction
width_connector=8; //in y direction
diameter_hole = 4.8;
distance_hole = 8;
length_connector = distance_hole+width_connector; //in x direction
length_beam = 30 +0.1;
width_beam = 1;
epsilon = 0.1;
$fn=50; //resolution



//first connector
connector();
//second connector at a distance length_beam - epsilon
//-epsilon is added so that the beam enters in the connectors
translate([length_connector+length_beam-epsilon,0,0])connector();
//the beam connects the two connectors
//-epsilon added so that the two parts connect
translate([(length_beam/2+distance_hole+width_connector/2-epsilon),0,0])cube([length_beam,width_beam,thickness_connector], true);

//connector design
module connector(){
difference() {hull(){
    translate([distance_hole,0,0]) cylinder(h=thickness_connector,d=width_connector,center=true);
    cylinder(h=thickness_connector,d=width_connector,center=true);
};
	cylinder (h = thickness_connector, d=diameter_hole, center = true);
	translate ([distance_hole,0,0]) cylinder (h = thickness_connector, d=diameter_hole, center = true);
}}

```





![connector1](../images/connector1.jpg)
###Cantilever beam

Le second est le ["Cantilever Beam"](https://www.compliantmechanisms.byu.edu/flexlinks).

Le code que j'ai utilisé est le suivant, il est disponible [ici](../files/FLEXLINKS/Fixed_fixedBeam_HalfCircle_lego.scad) :
```
//  FILE   : CantileverBeam.scad

//  AUTHOR : Stephanie Krins (stefkrins@gmail.com)

//  DATE   : 2021-02-23

//  LICENSE : Creative Commons [CC BY-4.0](https://creativecommons.org/licenses/by/4.0/).


//flexlink "Cantilever beam" from the BYU website
//(https://www.compliantmechanisms.byu.edu/flexlinks)
//1 connector connected with 1 long beam

//////////////
//DEFINITIONS
/////////////
thickness_connector = 6.4; //in z direction
width_connector=8; //in y direction
diameter_hole = 4.8;
distance_hole = 8;
length_connector = distance_hole+width_connector; //in x direction
length_beam = 100;
width_beam = 1;
epsilon=0.1;
$fn=50; //resolution



//first connector
connector();
//the beam starts in the connector (0.1 mm in the connector)
translate([(length_beam/2+distance_hole+width_connector/2-epsilon),0,0])cube([length_beam,width_beam,thickness_connector], true);

//connector design
module connector(){
difference() {hull(){
    translate([distance_hole,0,0]) cylinder(h=thickness_connector, d=width_connector,center=true);
    cylinder(h=thickness_connector,d=width_connector,center=true);
};
	cylinder (h = thickness_connector, d=diameter_hole, center = true);
	translate ([distance_hole,0,0]) cylinder (h = thickness_connector, d=diameter_hole, center = true);
}}

```
![cantilever](../images/cantilever.jpg)
###Fixed-fixed beam - Half Circle
Le troisième Flexlink que j'ai réalisé est le "Fixed-Fixed" avec un Beam en demi-cercle [ici](https://www.thingiverse.com/thing:3016943).

Le code est celui-ci, disponible [ici](../files/FLEXLINKS/CantileverBeam.scad) :
```
//  FILE   : Fixed-fixedBeam_HalfCircle_lego.scad

//  AUTHOR : Stephanie Krins (stefkrins@gmail.com)

//  DATE   : 2021-02-23

//  LICENSE : Creative Commons [CC BY-4.0](https://creativecommons.org/licenses/by/4.0/).


//flexlink "fixed-fixed beam half circle" from the BYU website
//(https://www.compliantmechanisms.byu.edu/flexlinks)
//2 connectors (with 2 holes) connected with a half circle beam

//////////////
//DEFINITIONS
/////////////

$fn=50;//resolution
epsilon = 0.1;

//connector variables
thickness_connector = 6.4; //in z direction
width_connector = 8; //in y direction
diameter_hole = 4.8;
distance_hole = 8;
length_connector = distance_hole+width_connector; //in x direction

//beam variables
distance_connectors=50; //will fix the length of the curved beam
width_beam=1; //in the xy plan
thickness_beam=thickness_connector; //in z direction



//first connector
connector();
//second connector
translate([0,distance_connectors,0])connector();

//the half-circle Beam
translate([length_connector-width_connector/2-epsilon,distance_connectors/2,0]){
difference(){
difference(){
//cylindre externe
cylinder(h=thickness_beam,d=distance_connectors,center=true);
//cylindre interne
cylinder(h=thickness_beam,d=distance_connectors-width_beam,center=true);}
//cube servant à couper
translate([-distance_connectors/2,0,0]){
cube([distance_connectors,distance_connectors, thickness_beam],center=true);}}}


//connector design
module connector(){
difference() {hull(){
    translate([distance_hole,0,0]) cylinder(h=thickness_connector,d=width_connector,center=true);
    cylinder(h=thickness_connector,d=width_connector,center=true);
};
	cylinder (h = thickness_connector, d=diameter_hole, center = true);
	translate ([distance_hole,0,0]) cylinder (h = thickness_connector, d=diameter_hole, center = true);
}}


```
![halfcircle](../images/halfcircle.jpg)

## Pièces compatibles LEGO
Pour que les pièces réalisées soient compatibles LEGO, j'ai utilisé les cotes reprises sur ce schéma de [Tips & Bricks](https://www.tipsandbricks.co.uk).
![lego_dimensions](../images/dimension_lego.jpg)

## Calibrateur des trous pour impression 3D
J'ai réalisé ce _kit de calibration de trous_ pour l'impression 3D, pour savoir quel diamètre de trou utiliser lors de l'impression des Flexlinks.
Celui du milieu a un diamètre de 4.8 mm (comme le mâle de LEGO). De part et d'autre, le diamètre varie par pas de 0.1 mm.

![calibrateur](../images/calibrateur_trou.jpg)

Voici le code du calibrateur de trous, disponible [ici](files/FLEXLINKS/width_hole_test_bench.scad) :
```
//  FILE   : width_hole_test_bench.scad

//  AUTHOR : Stephanie Krins (stefkrins@gmail.com)

//  DATE   : 2021-02-23

//  LICENSE : Creative Commons [CC BY-4.0](https://creativecommons.org/licenses/by/4.0/).

//width_hole test bench
//allows to check which hole diameter to choose for the flexlinks to be LEGO compatible

/////////////
//DEFINITIONS
/////////////

$fn=50; //resolution
thickness_connector = 6.4; //in z direction
width_connector=8; //in y direction
diameter_hole = 4.8; //diametre LEGO
distance_hole = 8; //distance LEGO


difference(){
hull(){
    translate([10*distance_hole,0,0]) cylinder(h=thickness_connector,d=width_connector,center=true);
    cylinder(h=thickness_connector,d=width_connector,center=true);}

    cylinder (h = thickness_connector, d=diameter_hole-0.5, center = true);
	translate ([distance_hole,0,0]) cylinder (h = thickness_connector, d=diameter_hole-0.4, center = true);
    translate ([2*distance_hole,0,0]) cylinder (h = thickness_connector, d=diameter_hole-0.3, center = true);
    translate ([3*distance_hole,0,0]) cylinder (h = thickness_connector, d=diameter_hole-0.2, center = true);
    translate ([4*distance_hole,0,0]) cylinder (h = thickness_connector, d=diameter_hole-0.1, center = true);
    translate ([5*distance_hole,0,0]) cylinder (h = thickness_connector, d=diameter_hole, center = true);
    translate ([6*distance_hole,0,0]) cylinder (h = thickness_connector, d=diameter_hole+0.1, center = true);
    translate ([7*distance_hole,0,0]) cylinder (h = thickness_connector, d=diameter_hole+0.2, center = true);
    translate ([8*distance_hole,0,0]) cylinder (h = thickness_connector, d=diameter_hole+0.3, center = true);
    translate ([9*distance_hole,0,0]) cylinder (h = thickness_connector, d=diameter_hole+0.4, center = true);
    translate ([10*distance_hole,0,0]) cylinder (h = thickness_connector, d=diameter_hole+0.5, center = true);

    }
```
>Après un test réel, je constate que le diamètre du trou du connecteur du Flexlink doit être de 5 mm pour être compatible LEGO. La distance entre les deux trous doit être de 8 mm.
