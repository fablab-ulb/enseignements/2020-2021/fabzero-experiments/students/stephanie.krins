//  FILE   : Fixed-fixedBeam_HalfCircle_lego.scad
    
//  AUTHOR : Stephanie Krins (stefkrins@gmail.com)
    
//  DATE   : 2021-02-23
    
//  LICENSE : Creative Commons [CC BY-4.0](https://creativecommons.org/licenses/by/4.0/).


//flexlink "fixed-fixed beam half circle" from the BYU website
//(https://www.compliantmechanisms.byu.edu/flexlinks) 
//2 connectors (with 2 holes) connected with a half circle beam

//////////////
//DEFINITIONS
/////////////

$fn=50;//resolution
epsilon = 0.1;

//connector variables
thickness_connector = 6.4; //in z direction
width_connector = 8; //in y direction
diameter_hole = 5;
distance_hole = 8;
length_connector = distance_hole+width_connector; //in x direction

//beam variables
distance_connectors=50; //will fix the length of the curved beam
width_beam=1; //in the xy plan
thickness_beam=thickness_connector; //in z direction



//first connector
connector(); 
//second connector
translate([0,distance_connectors,0])connector();

//the half circle beam
translate([length_connector-width_connector/2-epsilon,distance_connectors/2,0]){
difference(){
difference(){
//cylindre externe
cylinder(h=thickness_beam,d=distance_connectors,center=true);
//cylindre interne
cylinder(h=thickness_beam,d=distance_connectors-width_beam,center=true);}
//cube servant à couper
translate([-distance_connectors/2,0,0]){
cube([distance_connectors,distance_connectors, thickness_beam],center=true);}}}


//connector design
module connector(){ 
difference() {hull(){
    translate([distance_hole,0,0]) cylinder(h=thickness_connector,d=width_connector,center=true);
    cylinder(h=thickness_connector,d=width_connector,center=true);
};
	cylinder (h = thickness_connector, d=diameter_hole, center = true);
	translate ([distance_hole,0,0]) cylinder (h = thickness_connector, d=diameter_hole, center = true);
}}

