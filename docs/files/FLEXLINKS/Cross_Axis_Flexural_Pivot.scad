// file name : Cross_Axis_Flexural_Pivot.scad

//  AUTHOR : Stephanie Krins (stefkrins@gmail.com)
  
//  DATE   : 2021-03-02
    
//  LICENSE : Creative Commons [CC BY-4.0](https://creativecommons.org/licenses/by/4.0/).

// Code Adapted from the "Cross-Axis Flexural Pivot" of Floriane Weyer
//(https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/floriane.weyer/FabZero-Modules/module02/)

// PRINCIPALES MODIFICATIONS : 
// Je garde le code crée par Floriane pour définir une pièce en Z
//Je crée ensuite une pièce "en croix" en supperposant à une pièce en Z, une seconde pièce en Z, tournée de 180° selon l'axe x puis translatée en y.

// Paramètres
$fn=50;

// Hauteur de la pièce en Z
height = 3;

// Nombre de trous de la pièce en Z
n = 4;
// Rayon interne des trous
radius = 2.5;
// Distance centre à centre entre deux trous
distance = 8;
// Distance entre l'extérieur du trou et l'extérieur de la pièce
edge_thickness = 2;
//Longueur de la tête
head_length = (n-1)*distance + 2*edge_thickness + 2*radius;


//Distance selon y entre les deux têtes
head_distance = 40;

// Epaisseur de la tige
beam_thickness = 2;

// Calcul de la longueur de la tige et de l'angle
beam_length = sqrt(pow(head_distance,2)+pow((n-1)*distance,2))-(2*radius);
angle = atan(head_distance/((n-1)*distance));


//définition de la pièce en Z
module Z_flexlink(){
// Création de la première tête
head();
// Création de la deuxième tête
translate([0,head_distance,0]){
    head();
    }    
// Création de la première tige entre les deux têtes   
rotate([0,0,angle])   
translate([radius,-beam_thickness/2,0])
cube([beam_length,beam_thickness,height], center = True);}

//Création de la pièce en croix (superposition de deux pièces en Z en miroir)
// La pièce en Z n°1
Z_flexlink();
// La pièce en Z n°2
//Cette seconde pièce est tournée par rapport à la première de 180° selon l'axe x 
//et translatée en y de la distance entre le deux têtes (pour être parfaitement superposée à la pièce en Z n°1)
rotate(a=180, v=[1,0,0])
translate([0,-head_distance,0])
Z_flexlink();


//Définition du module head
module head(){
    difference(){
        hull(){
            cylinder(h = height, r = radius+edge_thickness, center = True);
            translate([(n-1)*distance,0,0]){cylinder(h = height, r = radius+edge_thickness, center = True);}
            }
        for (i = [1:n]){
            translate([(i-1)*distance,0,0]){
                cylinder(h = height, r = radius, center = True);
                }
            }
        }
    }