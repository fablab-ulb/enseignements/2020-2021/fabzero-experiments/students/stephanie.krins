//  FILE   : fixed-fixedBeam_lego.scad
    
//  AUTHOR : Stephanie Krins (stefkrins@gmail.com)
    
//  DATE   : 2021-02-23
    
//  LICENSE : Creative Commons [CC BY-4.0](https://creativecommons.org/licenses/by/4.0/).


//flexlink "fixed-fixed beam" from the BYU website
//(https://www.compliantmechanisms.byu.edu/flexlinks) 
//2 connectors (with 2 holes) connected with 1 beam


//////////////
//DEFINITIONS
/////////////
$fn=50; //resolution

thickness_connector = 6.4; //in z direction
width_connector=8; //in y direction
diameter_hole = 5;
distance_hole = 8;
length_connector = distance_hole+width_connector; //in x direction
length_beam = 30 +0.1;
width_beam = 1;
epsilon = 0.1;


//first connector
connector(); 
//second connector at a distance length_beam - epsilon
//-epsilon is added so that the beam enters in the connectors
translate([length_connector+length_beam-epsilon,0,0])connector(); 
//the beam connects the two connectors
//-epsilon added so that the two parts connect
translate([(length_beam/2+distance_hole+width_connector/2-epsilon),0,0])cube([length_beam,width_beam,thickness_connector], true); 

//connector design
module connector(){ 
difference() {hull(){
    translate([distance_hole,0,0]) cylinder(h=thickness_connector,d=width_connector,center=true);
    cylinder(h=thickness_connector,d=width_connector,center=true);
};
	cylinder (h = thickness_connector, d=diameter_hole, center = true);
	translate ([distance_hole,0,0]) cylinder (h = thickness_connector, d=diameter_hole, center = true);
}}

