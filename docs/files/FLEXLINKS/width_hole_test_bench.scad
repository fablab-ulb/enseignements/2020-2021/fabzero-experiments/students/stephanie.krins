//  FILE   : width_hole_test_bench.scad
    
//  AUTHOR : Stephanie Krins (stefkrins@gmail.com)
    
//  DATE   : 2021-02-23
    
//  LICENSE : Creative Commons [CC BY-4.0](https://creativecommons.org/licenses/by/4.0/).

//width_hole test bench
//allows to check which hole diameter to choose for the flexlinks to be LEGO compatible

/////////////
//DEFINITIONS
/////////////

$fn=50; //resolution
thickness_connector = 6.4; //in z direction
width_connector=8; //in y direction
diameter_hole = 4.8; //diametre LEGO
distance_hole = 8; //distance LEGO


difference(){
hull(){
    translate([10*distance_hole,0,0]) cylinder(h=thickness_connector,d=width_connector,center=true);
    cylinder(h=thickness_connector,d=width_connector,center=true);}
    
    cylinder (h = thickness_connector, d=diameter_hole-0.5, center = true);
	translate ([distance_hole,0,0]) cylinder (h = thickness_connector, d=diameter_hole-0.4, center = true);
    translate ([2*distance_hole,0,0]) cylinder (h = thickness_connector, d=diameter_hole-0.3, center = true);
    translate ([3*distance_hole,0,0]) cylinder (h = thickness_connector, d=diameter_hole-0.2, center = true);
    translate ([4*distance_hole,0,0]) cylinder (h = thickness_connector, d=diameter_hole-0.1, center = true);
    translate ([5*distance_hole,0,0]) cylinder (h = thickness_connector, d=diameter_hole, center = true);
    translate ([6*distance_hole,0,0]) cylinder (h = thickness_connector, d=diameter_hole+0.1, center = true);
    translate ([7*distance_hole,0,0]) cylinder (h = thickness_connector, d=diameter_hole+0.2, center = true);
    translate ([8*distance_hole,0,0]) cylinder (h = thickness_connector, d=diameter_hole+0.3, center = true);
    translate ([9*distance_hole,0,0]) cylinder (h = thickness_connector, d=diameter_hole+0.4, center = true);
    translate ([10*distance_hole,0,0]) cylinder (h = thickness_connector, d=diameter_hole+0.5, center = true);
   
    }